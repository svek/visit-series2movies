# Batch rendering with Visit

This repository contains a small python script to simplify mass creation
of videos with the [Visit visualization code](https://wci.llnl.gov/simulation/computer-codes/visit).

See also the [Making Movies](https://www.visitusers.org/index.php?title=Making_Movies) guide
in the VisItusers wiki.

Actually, recent versions of VisIt already allow a decent level of batch processing.
Nevertheless, the actual movie maker in Visit is written in Python, nevertheless.
So this whole script is a bit overkill.

## Status

The script itself works, but Visit does not. With my data it always segfaults.
Is probably related to OpenGL (while I also tested the MESA version and it did
not work).