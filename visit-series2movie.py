#!/usr/bin/python
# Python2
# Modules: Needs pyquery
#
# Purpose/Usage: Open visit, load one of your CarpetHDF5 files and 
#   handle all the display like you whish. Then click
#   export movie, store to a movie.session file.
#   The movie.session XML file is used as template for this python script.
#   Then, use this script to batch export multiple CarpetHDF5 files
#   in a similiar way to a movie.
#   This will then create a bunch of movie.session XML files along with
#   command scripts which you can run subsequently in parallel or serially
#   to generate the movies.
#   You can delete the XML and shell scripts afterwards.
#
# SvenK, 2018-01-07

from pyquery import PyQuery

## stuff to set
visit_bin = "/home/sven/bin/visit2_13_0.linux-x86_64/bin/visit"

# python internals (batteries):
import argparse, io, sys, os, re
from lxml import etree
from collections import OrderedDict
from os.path import abspath, basename, splitext, dirname, exists, join
first = lambda l: l[0]
removeChars = lambda chars, text: re.sub(chars, '', text)
def log(t): print t # future compatible

# helper functions
class BadVisitXMLWorkaround:
	"""
	Repairs broken XML given out by Visit. This are texts such as
	  <Field name="definition" type="string">"cell_constant(\<Carpet AMR-grid\>, 0.)"</Field>
	Here, \< and \> are illegal XML constructs. They probably should escape XML,
	the proper escape sequence would actually have been &lt; and &gt;
	Instead, we replace them with some magic here.
	"""
	def __init__(self):
		self.repl = {
			r'\<': "__SV_LT_MASK__",
			r'\>': "__SV_GT_MASK__",
			r'&':  "__SV_AMP_MASK__"
		}

	def mask(self, xmltext):
		for bad,good in self.repl.iteritems():
			xmltext = xmltext.replace(bad, good)
		return xmltext

	def unmask(self, xmltext):
		for bad,good in self.repl.iteritems():
			xmltext = xmltext.replace(good, bad)
		return xmltext

class VisitMovieMaker:
	def __init__(self, h5filename, require_exist=True):
		self.fname = h5filename

		# full path for input in XML
		self.source_filename = abspath(h5filename)
		self.exists = exists(self.source_filename)
		if require_exist:
			if not self.exists:
				raise ValueError("Input CarpetHDF5File '%s' does not exist" % self.source_filename)

		# full path for output directory for movie
		self.output_directory = dirname(self.source_filename)
		# basename for movie
		self.output_file_basename = join(self.output_directory, first(splitext(h5filename)))

		# full path excluding the h5 extension
		self.full_path = join(self.output_directory, self.output_file_basename)
		# output filename for visit session file
		self.output_visit_session = self.full_path + ".visit.session"
		# Relative path, for test in script
		self.output_visit_session_relative = basename(self.full_path + ".visit.session")
		# output filename for generator
		self.output_visit_jobscript = self.full_path + ".visit.sh"
	
	def gen_visit_session(self, sessionfilehandle):
		# preparing the Visit movie.session file
		sessiontpl = sessionfilehandle.read()
		badvisit = BadVisitXMLWorkaround()
		goodxml_in = badvisit.mask(sessiontpl)
		root = etree.fromstring(goodxml_in)
		d = PyQuery(root, parser='xml')

		url_source_filename = 'localhost:' + self.source_filename
		# modification of XML
		d("Object[name=SourceMap] Field").text(url_source_filename)
		d("Object[name=SourcePlugins] Field").attr('name', url_source_filename)
		d("Object[name=MovieAttributes] Field[name=outputDirectory]").text(self.output_directory)
		d("Object[name=MovieAttributes] Field[name=outputName]").text(self.output_file_basename)

		self.d = d # store for later
		#firstline = '<?xml version="1.0"?>' + "\n"
		#goodxml_out = firstline + d.html()#method='xml')
		goodxml_out = etree.tostring(root, encoding='utf-8', xml_declaration=True, pretty_print=True)
		badvisit_out = badvisit.unmask(goodxml_out)
		return badvisit_out

	def write_visit_session(self, sessionfilehandle):
		sessionfilecontent = self.gen_visit_session(sessionfilehandle)
		log("Writing visit session info to %s ..." % self.output_visit_session)
		with open(self.output_visit_session, 'w') as ofh:
			ofh.write(sessionfilecontent)

	def gen_visit_jobscript(self):
		"""
		Comes up with a command line call to visit to generate the video
		as requested in the session file. Example output is like
		
		visit -movie -v 2.10.3 -format mpeg -geometry 1284x988 -output /home/sven/movie -fps 10 -start 0 -end 199 -frame 0 -sessionfile /home/sven/movie.session 
		"""
		arg = OrderedDict()
		arg['movie'] = ''  # flag argument requires no value string
		# visit version string like 2.10.3
		arg['v'] = self.d("Field[name=Version]").text()

		# Known limitation: Visit allows to export into several movie formats at a single go.
		# Here we only support to extract the parameters of a single format.
		attr = self.d("Object[name=MovieAttributes]")
		field = lambda name: attr.find("Field[name=%s]" % name).text().strip()

		arg['format'] = removeChars('"', field('fileFormats')) # e.g. "mpeg"
		arg['geometry'] = field('widths') + 'x' + field('heights') # e.g. 1284x975
		arg['output'] = self.output_file_basename
		arg['fps'] = field("fps")
		arg['start'] = field('startIndex')
		arg['end'] = field('endIndex')
		arg['frame'] = field('initialFrameValue')
		arg['sessionfile'] = self.output_visit_session_relative

		#visit_bin = "visit"
		global visit_bin
		# join the arg dict to "-key value" pairs
		visit_args = ["-%-13s %s" % kv for kv in arg.iteritems()]

		nl = "\n"
		shf  = '#!/bin/sh' + nl*2
		shf += "cd " + self.output_directory + " || exit 1" + nl*2
		indent = ' '*5
		linebreak = "\\" + nl
		shf += visit_bin + linebreak
		shf += linebreak.join([indent + a for a in visit_args])
		shf += nl*2
		return shf

	def write_visit_jobscript(self):
		jobscriptcontent = self.gen_visit_jobscript()
		log("Writing shell script to %s ..." % self.output_visit_jobscript)
		with open(self.output_visit_jobscript, 'w') as ofh:
			ofh.write(jobscriptcontent)
		# chmod +x output_visit_jobscript
		st = os.stat(self.output_visit_jobscript)
		os.chmod(self.output_visit_jobscript, st.st_mode | 0111)


# main program start
parser = argparse.ArgumentParser()
parser.add_argument('movietemplatefile', type=argparse.FileType('r'), help="A movie.session file generated by Visit")
parser.add_argument('h5files', nargs='+', help="One or more CarpetHDF5 files")

args = parser.parse_args()

# preparing the makers
moviemakers = [ VisitMovieMaker(h5filename) for h5filename in args.h5files ]

# do the work
for m in moviemakers:
	m.write_visit_session(args.movietemplatefile)
	m.write_visit_jobscript()

